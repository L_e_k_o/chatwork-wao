var fs     = require('fs');
var gulp   = require('gulp');
var gutil  = require('gulp-util');
var sass   = require('gulp-sass');
var rename = require('gulp-rename');
var resize = require('gulp-image-resize');
var coffee = require('gulp-coffee');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rimraf = require('gulp-rimraf');

var paths = {
  sass: ['./source/scss/**/*.scss'],
  coffee: ['./source/coffee/*.coffee'],
  icon: {
    original : 'source/icon/icon.png'
  },
  dist: './dist'
};

/** 
 * 複数サイズのアイコンを生成する
 * 生成されるアイコンは、_dest + _sizes[iconName]というパスになります。
 * 固定のパスは_destに、サイズごとに変化するパスは_sizesに指定してください。
 * @param string original サイズのバリエーションを作成する際のオリジナル画像ファイルのパス
 * @param string _dest    バリエーションを吐き出す共通パス。destだと名前が衝突しそうなのでアンスコをつけて回避
 * @param object sizes    ファイル名: 1辺のサイズというオブジェクトを与える
 * @return void
 */
function generateIcons(original, _dest, sizes) {
  var cyanNote = gutil.colors.cyan('NOTE: ');

  // ファイルの存在チェック、なければエラーをコンソールに出して終了
  if(!fs.existsSync(original)) {
    console.log(cyanNote + original + 'がありません。');
    console.log(cyanNote + 'icon.pngは1024x1024ピクセルで作成してください。');

  // オリジナルファイルがあれば必要なサイズ分だけ画像のコピーを生成
  } else {
    for(var iconName in sizes) {
      var size = sizes[iconName];
      if(!sizes.hasOwnProperty(iconName)) continue;

      gulp.src(original)
        .pipe(resize({ width: size, height: size, imageMagick: true }))
        .pipe(rename(iconName))
        .pipe(gulp.dest(_dest));

      // 作成したファイルのパスをコンソールに表示
      console.log(cyanNote + 'create icon ' + _dest + '/' + iconName);
    }
  }
}

/**
 * icon-iosとicon-androidをまとめて実行する
 * @task icon
 */
gulp.task('icon', function() {
  var _dest = paths.dist;

  var sizes = {
    'icon16.png': 16,
    'icon48.png': 48,
    'icon128.png': 128,
  };

  generateIcons(paths.icon.original, _dest, sizes);
});

/**
 * sourceディレクトリの中のcoffeeファイルを１ファイルに結合する
 * @task coffee-concat
 */
gulp.task('coffee-concat', function() {
  return gulp.src(paths.coffee)
    .pipe(concat('app.coffee'))
    .pipe(gulp.dest(paths.dist))
});

/**
 * coffee scriptをコンパイルする
 * @task coffee-compile
 */
gulp.task('coffee-compile', function() {
  return gulp.src(paths.dist + '/app.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(rename('app.js'))
    .pipe(gulp.dest(paths.dist));
});

/**
 * 
 * @task uglify
 */
gulp.task('uglify', function() {
  return gulp.src([
      './source/bower/lodash/dist/lodash.underscore.min.js',
      './source/bower/zepto/zepto.js',
      './source/bower/zepto/selector.js',
      './source/bower/zepto/event.js',
      './source/bower/backbone.js',
      paths.dist + '/app.js'
    ])
    .pipe(uglify({ preserveComments: 'all', compress: true }))
    .pipe(rename('app.min.js'))
    .pipe(gulp.dest(paths.dist));
});

/**
 * 
 * @task sass
 */
gulp.task('sass', function() {
  gulp.src(paths.sass)
    .pipe(sass({ sync: true }))
    .pipe(rename('app.min.css'))
    .pipe(gulp.dest(paths.dist));
});

gulp.task('manifest', function() {
  var VERSION = 2,
      TARGET_DOMAIN  = 'https://www.chatwork.com/*';

  // FIXME: gulpで特定のファイル内容をパースして任意の内容を改変してファイル書き出しする方法がわからない
  var manifest = {
        icons: {},
        content_scripts: []
      },
      package = JSON.parse(fs.readFileSync('./package.json'));

  manifest.name             = package.name;
  manifest.version          = package.version;
  manifest.manifest_version = VERSION;
  manifest.description      = package.description;
  manifest.icons['16']      = paths.dist + '/icon16.png';
  manifest.icons['48']      = paths.dist + '/icon48.png';
  manifest.icons['128']     = paths.dist + '/icon128.png';
  manifest.content_scripts.push({
    matches: [TARGET_DOMAIN],
    js     : [paths.dist + '/app.min.js'],
    css    : [paths.dist + '/app.min.css'],
  });

  fs.writeFileSync('manifest.json', JSON.stringify(manifest, null, 4));
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.coffee, ['coffee']);
  gulp.watch('./package.json', ['manifest']);
});

gulp.task('coffee', ['coffee-concat', 'coffee-compile', 'uglify'], function() {
  gulp.src([
      paths.dist + '/app.coffee',
      paths.dist + '/app.js',
    ])
    .pipe(rimraf({ force: true }));
});

gulp.task('default', ['sass', 'icon', 'sass', 'coffee', 'manifest']);
